//
//  problem.c
//  
//
//  Created by Fernando Pérez on 2015-10-22.
//
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14159265358979323846

int main(void){
    long double w; //Width of the sheet of paper.
    long double h; //Height of the sheet of paper.
    long double height; //Perimeter of the cylinder.
    long double perimeter; // Perimeter of the circle.
    long double radius; //Maximum radius of the circle.
    long double volume; //Maximum volume of the cylinder.
    
    while(1){
        //Read w and h.
        scanf("%Lf %Lf",&w,&h);
        //Stops if w = h = 0.
        if((w == 0) && (h == 0)){
            break;
        }
        
        //Now, cut the paper horizontally(parallel to the shorter side). This
        //means that the larger side will be divided by 2.
        if(w >= h){
            w = (long double) w / (long double) 2.00;
        }
        else{
            h = (long double) w / (long double) 2.00;
        }
        
        //Then, the maximum radius of the circle is the smallest values between
        //w and h divided by two.
        if(w >= h){
            radius = (long double) h * ;
        }
        else{
            radius = (long double) w * ;
        }
        
        printf("%Lf\n",radius);
        
        //Determine perimeter.
        perimeter = 2 * PI * radius;
        
        //Then, determine height, the formula of the area of a cylinder is
        //2 * pi * h, and since the perimeter of the circle is the same than the
        // area of the cylinder, h = perimeter / (2 * PI)
        height = (long double) perimeter / (long double) (2 * PI);
        
        //Then, the volume of the cylinder is 2 * PI * radius * height.
        volume = 2 * PI * radius * height;
        printf("%.3Lf\n",volume);
    }
    
    
    
    return(0);
}