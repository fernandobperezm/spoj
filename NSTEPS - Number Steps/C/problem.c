/*
 * problem.c
 *
 * Description: Solves the Number steps problem. Problem available at:
 * http://www.spoj.com/problems/NSTEPS/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ user: fernandobperezm.
 *
 * Last Modified: 2015-10-22.
 */

#include <stdio.h>
#include <stdlib.h>

int main(void){
    int N; //Number of test cases for this problem.
    int x; //Coordinate x.
    int y; //Coordinate y.
    int i; //iterator
    int integer;
    
    //Read number of test cases.
    scanf("%d",&N);
    for(i = 0;i < N;i++){
        //Read x and y.
        scanf("%d %d",&x,&y);
        getchar(); //Remove newline char.
        //Verification. If (x,y) represents an integer, then the formula for this
        //integer is: if x,y % 2 = 0 then integer = x + y, else of x,y % 2 = 1
        //then integer = x + y - 1.
        
        //(x,y) represents an integer if x- y = 2 or x- y = 0. or x + y % 2 = 1.
        
        //Check if (x,y) represents an integer.
        if((x - y == 2) || (x-y == 0)){
            //Check if we have a odd or even pair).
            if((x % 2 == 0) && (y % 2 == 0)){
                integer = x + y;
                printf("%d\n",integer);
            }
            else if((x % 2 == 1) && (y % 2 == 1)){
                integer = x + y - 1;
                printf("%d\n",integer);
            }
            else{
                printf("No Number\n");
            }
        }
        else{
            printf("No Number\n");
        }
    }
    return(0);
}