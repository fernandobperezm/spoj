/**
 * problem.java
 *
 * Description: Solves the Hello Kitty problem. Problem available at:
 * http://www.spoj.com/problems/HELLOKIT/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ username: fernandobperezm.
 *
 * Last Modified: 2015-10-10.
 */
 
import java.util.*;
import java.lang.*;
import java.io.*;

class Main{
    public static void main(String[] args) throws java.lang.Exception{
        //Defines a new Scanner for standard input.
        Scanner s = new Scanner(System.in);
         /*
          enter: the number of repetitions.
          str: the word to repeat.
          largo: length of str.
          repetir: largo * enter.
          i,j counters for iterations.
          array: str converted to char array.
          modulo: position of the array to print, it is set to 0 following the 
          modulo sum by the length of the word.
          */
        int enter = 0;
        String str = "";
        int largo = 0;
        int i;
        int repetir;
        int j;
        char[] array;
        int modulo;
        
        //Iteration.
        while(true){
            str = s.next();
            array = str.toCharArray();
            //Stop if found . char.
            if(array[0] == '.'){
                break;
            }
            
            //Get the numbers of times that the word is repeated, then get the
            //length of the word and set modulo = 0 in every loop.
            enter = s.nextInt();
            largo = str.length();
            modulo = 0;
            repetir = largo * enter;
            /*
             2 fors to print out the word. The first one is the numbers of lines. 
             The second one is the number of times that the word is repeated at
             the same line.
             */
            for(i = 0;i < largo;i++){
                for(j = 0;j < repetir;j++){
                    //Print out the current char at modulo position.
                    System.out.print(array[modulo]);
                    modulo++;
                    if (modulo % largo == 0){
                        modulo = 0;
                    }
                }
                //print a newline and add one to modulo to begin with the next char.
                System.out.print("\n");
                modulo++;
            }
        }
        //Close the Scanner.
        s.close();
    }
}