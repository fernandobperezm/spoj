/**
 * problem.cpp
 *
 * Description: Solves the Hello Kitty problem. Problem available at:
 * http://www.spoj.com/problems/HELLOKIT/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ username: fernandobperezm.
 *
 * Last Modified: 2015-10-15.
 */

#include <iostream>
#include <string>

using namespace std;

int main(void){
    /*
      str: the string to output.
      enter: the number of times to be repeated at the same line.
      largo: length of str.
      repetitions: number of chars to be printed in a single line.
      modulo: current char to print.
    */
    string str;
    int enter;
    int largo;
    int repetitions;
    int modulo;
    
    //Starting to read the words.
    while(1){
        //Read the word.
        cin >> str;
        //If a dot is readed, finish.
        if(str.compare(".") == 0){
            break;
        }
        //Read the number.
        cin >> enter;
        
        largo = str.length();
        repetitions = enter * largo;
        modulo = 0;
        /*
         Begin the output of the problem. The second for is to print the word; repeated,
         in a single line, the first for is to change the line of output.
         */
        for(int i = 0;i < largo;i++){
            for(int j = 0; j < repetitions; j++){
                cout << str[modulo];
                modulo++;
                if( modulo % largo == 0){
                    modulo = 0;
                }
            }
            cout << "\n";
            modulo++;
        }
    }
    return(0);
}