/*
 * problem.c
 *
 * Description: Solves the count on cantor problem. Problem available at:
 * http://www.spoj.com/problems/CANTON/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ user: fernandobperezm.
 *
 * Last Modified: 2015-10-22.
 */

#include <stdio.h>
#include <stdlib.h>

int main(void){
    int t; //Number of test cases.
    int x; //Rows on cantor's diagram.
    int y; //Columns on cantor's diagram.
    int step; //Steps to iterate.
    int test; // Test case.
    int i; //iterator.
    int k; //auxiliar int.
    int sem;
    
    //Read t.
    scanf("%d",&t);
    
    for(i = 0;i < t; i++){
        //Read test case.
        scanf("%d",&test);
        
        //Set number of steps required to get the term.
        step = 0;
        k = 0;
        while(step < test){
            k++;
            if(k % 2 == 0){
                x = 0;
                y = k + 1;
                for(sem = 0;sem < k;sem++){
                    x++;
                    y--;
                    step++;
                    if(step == test){
                        break;
                    }
                }
            }
            else{
                x = k+1;
                y = 0;
                for(sem = 0;sem < k;sem++){
                    x--;
                    y++;
                    step++;
                    if(step == test){
                        break;
                    }
                }
            }
            
        }
        printf("TERM %d IS %d/%d\n",test,x,y);
    }
    
    
    return(0);
}
