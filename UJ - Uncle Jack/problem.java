import java.math.BigInteger;
import java.util.Scanner;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n,d;
        BigInteger respuesta;

        while(true){
            n = sc.nextInt();
            d = sc.nextInt();

        if (n == 0 && d == 0){
            break;
        }

        respuesta = BigInteger.ONE;
        for (int i = 0;i<d;i++){
            respuesta = respuesta.multiply(BigInteger.valueOf(n));
        }

        System.out.println(respuesta);

        }

        sc.close();
    }
}


