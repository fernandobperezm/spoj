/*
 * problem.c
 *
 * Description: Solves the triple fat ladies problem. Problem available at:
 * http://www.spoj.com/problems/EIGHTS/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ user: fernandobperezm.
 *
 * Last Modified: 2015-10-22.
 */
#include <stdio.h>

int main(void){
    /* 
      By printing each number whose cube ends in 888 in Python, there's a closed
      formula to determine the kth number whose cube ends in 888. This formula is:
      192 + (k-1)*250
     */
    int t; //number of test cases.
    //k is long double type because of float overflow.
    long double k; // the kth number whose cube ends in 888.
    int i; //iteration.
    
    //Read t.
    scanf("%d",&t);
    for(i = 0 ; i < t; i++){
        //read k.
        scanf("%Lf",&k);
        k = 192 + ((k - 1)*250);
        printf("%.0Lf\n",k);
    }
    return(0);
    
}