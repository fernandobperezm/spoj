/*
 * problem.c
 *
 * Description: Solves the count on cantor problem. Problem available at:
 * http://www.spoj.com/problems/ADDREV/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ user: fernandobperezm.
 *
 * Last Modified: 2015-11-13.
 */

#include <stdio.h>
#include <stdlib.h>

int main(void){
    int n; //Number of test cases.
    int firstInt; //First number to reverse.
    int secondInt; //Second number to reverse.
    int sum; //Sum of the reversed numbers.
    int i; //iterator.
    
    //Read n.
    scanf("%d",&n);
    
    for(i = 0;i < n; i++){
        //Read test case.
        scanf("%d %d",&firstInt,&secondInt);
        //Reverse both numbers.
		firstInt = reverseNumber(firstInt);
		secondInt= reverseNumber(secondInt);
		
		sum = firstInt + secondInt;
		
		sum = reverseNumber(sum);
		
		printf("%d\n",sum);
    }
	    
    return(0);
}

int reverseNumber(int number){
	int modulo;
	int aux = number;
	int reversed = 0;
	int tenFactor = 1;
	
	/*Gets the 10 factor of the number*/
	while(aux != 0){
		if(aux < 10){
			break;
		}
		aux = aux / 10;
		tenFactor = tenFactor * 10;
	}

	/*
		To reverse the number, I get the last position integer making a 
		modulo operation. Then, the number is divided by 10. The reversed
		number is the sum of reversed + modulo*tenFactor. Stops when number == 0.
	*/
	while(number != 0){
		modulo = number % 10; //Gets the last position integer.
		number = number / 10;
		reversed += modulo*tenFactor;
		tenFactor = tenFactor / 10;	
	}
	
	return reversed;
}

