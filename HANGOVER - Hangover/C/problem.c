/*
 * problem.c
 *
 * Description: Solves the hangover problem. Problem available at:
 * http://www.spoj.com/problems/HANGOVER/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ user: fernandobperezm.
 *
 * Last Modified: 2015-10-19.
 */

#include <stdio.h>
#include <stdlib.h>

int main(void){
    int n; //Numbers of cards.
    //Using long doubles to have the most accurate calculations.
    long double acc; //acc for calculations.
    long double c; //card length on each test case.
    
    while(1){
        //read input.
        scanf("%Lf",&c);
        getchar();
        //Stop if c = 0.00
        if( c > 0.00){
            //initialize acc.
            acc = (long double) 0.0;
            n = 1;
            while(acc < c){
                n++;
                acc += (long double)1.00 / (long double) n;
            }
            
            //After, print out how many cards are needed.
            n = n-1;
            printf("%d card(s)\n",n);
        }
        else{
            break;
        }
        
    }
    return(0);
}
