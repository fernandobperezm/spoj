/*
 * problem.cpp
 *
 * Description: Solves the Offside problem. Problem available at:
 * http://www.spoj.com/problems/OFFSIDE/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ username: fernandobperezm
 *
 * Last Modified: 2015-10-17.
 */

#include <iostream>

using namespace std;

int main(void){
	int A; //Number of attacking players.
	int D; //Number of defending players.
	int B; //Position of current attacking player.
	int C; //Position of current defending player.
	int i; // iterator.
	int firstA; // Position of the leadest attacker.
	int firstD; // Position of the leadest defender.
	int secondD; // Position of the second leadest defender.
	
	while(1){
		//Reading A and D.
		cin >> A;
		cin >> D;
		//Check if end of input.
		if( (A == 0 ) && (D == 0) ){
			break;
		}
		//Read B.
		firstA = 10000;
		for(i = 0;i < A; i++){
			cin >> B;
			if(firstA >= B){
				firstA = B;
			}
		}
		
		//Read C.
		firstD = 10000;
		secondD = 10000;
		for(i = 0; i < D; i++){
			cin >> C;
			if(firstD > C){
                secondD = firstD;
				firstD = C;
			}
            else if(secondD > C){
                secondD = C;
            }
            else{
                
            }
		}
		
		if(firstA < secondD){
			cout << 'Y';
		}
		else{
			cout << 'N';
		}

		cout << "\n";
	}
	
	return(0);
}