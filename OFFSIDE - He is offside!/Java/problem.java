/**
 * problem.java
 *
 * Description: Solves the Offside problem. Problem available at:
 * http://www.spoj.com/problems/OFFSIDE/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ username: fernandobperezm.
 *
 * Last Modified: 2015-10-17.
 */

import java.util.*;
import java.lang.*;
import java.io.*;

class Main{
    public static void main(String[] args) throws java.lang.Exception{
        int A; //Number of attacking players.
        int D; //Number of defending players.
        int B; //Field position of current attacker.
        int C; //Field position of current defender.
        int firstA; //Closest attacker.
        int firstD; //Closest defender.
        int secondD; //Second closest defender.
        Scanner scanner = new Scanner(System.in); //Initialize scanner for I/O.
        
        //Read all input.
        while(true){
            //Read A and D.
            A = scanner.nextInt();
            D = scanner.nextInt();
            //Stop if A = D = 0.
            if(A == D && D == 0){
                break;
            }
            
            //Read B.
            firstA = 10000;
            for(int i = 0;i < A; i++){
                B = scanner.nextInt();
                if(firstA > B){
                    firstA = B;
                }
            }
            
            //Read C.
            firstD = 10000;
            secondD = 10000;
            for(int i = 0; i < D; i++){
                C = scanner.nextInt();
                if(firstD > C){
                    secondD = firstD;
                    firstD = C;
                }
                else if( secondD > C){
                    secondD = C;
                }
                else{
                }
            }
            
            if(firstA < secondD){
                System.out.println('Y');
            }
            else{
                System.out.println('N');
            }
        }
        scanner.close();

    }
}