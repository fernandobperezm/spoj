





import java.io.*;
import java.lang.*;
import java.util.*;

class Main{
    public static void main(String[] args){
        //Scanner for IO.
        Scanner scanner = new Scanner(System.in);
        String identifier = "";
        char[] characters = new char[100];
        int size;
        //Defines c++ patterns.
        //String cIdentifiers = "[a-z]+([_][a-z]+)*[a-z]";
        //String javaIdentifiers = "[a-z][a-zA-Z]*";
        //String[] words = new String[100];
        
        while(scanner.hasNextLine()){
            //read the identifier.
            identifier = scanner.nextLine();
            if(identifier.equals("")){
                break;
            }
            
            characters = identifier.toCharArray();
            size = identifier.length();
            
            Character w = characters[0];
            //If the first letter is upper case or isn't a letter then error.
            if(Character.isUpperCase(w) || !Character.isLetter(w)){
                System.out.println("Error!");
            }
            else{
                //Check if it's java or cpp identifier.
                if(identifier.equals(identifier.toLowerCase())){
                    //We are in cpp identifier.
                    //Need to check if there are two or more _.
                    boolean seenUnder = false;
                    for(int i = 1;i<size;i++){
                        w = characters[i];
                        if(w.equals('_')){
                            if(seenUnder){
                                System.out.println("Error!");
                                break;
                            }
                            else{
                                seenUnder = true;
                            }
                        }
                        else if(Character.isLetter(w)){
                            if(seenUnder){
                                System.out.print(w.toString().toUpperCase());
                            }
                            else{
                                System.out.print(w.toString());
                            }
                        }
                        else{
                            System.out.println("Error!");
                            break;
                        }
                        
                    }
                    //TODO: VERIFY THAT THERE'S NO NUMBERS IN THE INPUT.
                }
                else{
                    //we are in java identifier.
                    
                    if(identifier.contains("_")){
                        System.out.println("Error!");
                    }
                    else{
                        //We have a correct java word, we need to transform it.
                        System.out.print(w);
                        for(int i = 1;i<size;i++){
                            w = characters[i];
                            if(Character.isLetter(w)){
                                if(Character.isUpperCase(w)){
                                    System.out.print("_");
                                    System.out.print(w.toString().toLowerCase());
                                }
                                else{
                                    System.out.print(w);
                                }
                            }
                            else{
                                System.out.println("Error!");
                                break;
                            }
                            
                        }
                        System.out.println();
                    }
                }

                
            }
            
            
        }
        scanner.close();
    }
}

