/*
 * problem.c
 *
 * Description: Solves the candy problem. Problem available at:
 * http://www.spoj.com/problems/CANDY/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ user: fernandobperezm.
 *
 * Last Modified: 2015-10-17.
 */

#include <stdio.h>
#include <stdlib.h>

int main(void){
    int N; //number of candy packets.
    int candies[10000]; //number of candies in each packet.
    int i; //iterator.
    int acc; //sums all candies.
    int steps; //sums the steps.
    int sameNumber; //Equally number of candies in each packet.
    
    while(1){
        //Read N.
        scanf("%d",&N);
        
        //Stop if read -1.
        if(N == -1){
            break;
        }
        
        //Read candies.
        acc = 0;
        for(i = 0; i < N; i++){
            scanf("%d",&(candies[i]));
            acc += candies[i];
        }
        
        //Check if it's possible to have the same number of candies in each packet.
        sameNumber = acc % N;
        if(sameNumber != 0){
            N = -1;
            printf("%d\n",N);
        }
        else{
            //To sum the steps, if candies[i] < sameNumber, then the difference is
            //added to steps, else, skip.
            steps = 0;
            sameNumber = acc / N;
            for(i = 0;i < N; i++){
                if(candies[i] < sameNumber){
                    steps = steps + (sameNumber - candies[i]);
                }
            }
            //Print out the number of steps.
            printf("%d\n",steps);
        }
    }
    return 0;
}