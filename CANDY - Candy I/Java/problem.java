
/*
 * problem.java
 *
 * Description: Solves the candy problem. Problem available at:
 * http://www.spoj.com/problems/CANDY/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ user: fernandobperezm.
 *
 * Last Modified: 2015-10-17.
 */

import java.util.*;
import java.lang.*;
import java.io.*;

class Main{
    public static void main(String[] args) throws java.lang.Exception{
        int N; //number of candy packets.
        int[] candies = new int[10000]; //number of candies in each packet.
        int acc; //sums all candies.
        int steps; //sums the steps.
        int sameNumber; //Equally number of candies in each packet.
        Scanner scanner = new Scanner(System.in); //Scanner for I/O.
        int i; //iterator.

        while(true){
            //Read N.
            N = scanner.nextInt();

            //Stop if read -1.
            if(N == -1){
                break;
            }

            //Read candies.
            acc = 0;
            for(i = 0; i < N; i++){
                candies[i] = scanner.nextInt();
                acc += candies[i];
            }

            //Check if it's possible to have the same number of candies in each packet.
            sameNumber = acc % N;
            if(sameNumber != 0){
                N = -1;
                System.out.println(N);
            }
            else{
                //To sum the steps, if candies[i] < sameNumber, then the difference is
                //added to steps, else, skip.
                steps = 0;
                sameNumber = acc / N;
                for(i = 0;i < N; i++){
                    if(candies[i] < sameNumber){
                        steps = steps + (sameNumber - candies[i]);
                    }
                }
                //Print out the number of steps.
                System.out.println(steps);
            }
        }
        scanner.close();
    }
}