/*
 * problem.c
 *
 * Description: Solves the Test problem. Available at: 
 * http://www.spoj.com/problems/TEST/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ username: fernandobperezm.
 *
 * Last Modified: 2015-10-10.
 *
 */

#include <stdio.h>
#include <stdlib.h>

int main (void){
    int enter;
    while (1){
        scanf("%d",&enter);
        if (enter == 42){
            break;
        }
        printf("%d\n",enter);
    }
    return 0;
}