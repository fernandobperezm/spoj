/*
 * problem.java
 *
 * Description: Solves the Test problem. Available at:
 * http://www.spoj.com/problems/TEST/
 *
 * Created by: Fernando Benjamín Pérez Maurera.
 * SPOJ username: fernandobperezm.
 *
 * Last Modified: 2015-10-10.
 *
 */

import java.util.*;
import java.lang.*;
import java.io.*;

class Main{
    public static void main(String[] args) throws java.lang.Exception{
        //Defines a new Scanner for standart input.
        Scanner s = new Scanner(System.in);
        int enter = 0;
        while(s.hasNextInt()){
            enter = s.nextInt();
            if(enter == 42){
                break;
            }
            System.out.println(enter);
        }
        s.close();
    }
}